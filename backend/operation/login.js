let promise = require('promise');
let UserSchema = require('../model/userModel');

let userFound = false;
let pwd = "";
let checkUser = (parameter)=>{
    return new promise((resolve, reject)=>{
        UserSchema.find({user_name:parameter.userName})
        .exec()
        .then(function(result){
            pwd = result.user_password;
            if(result.user_password == pwd){
                userFound = true
            }
            resolve(userFound);
        }).catch(function(err){
            reject('User Not found : '+err);
        })
    });
};


module.exports ={
    checkUser : checkUser
}


