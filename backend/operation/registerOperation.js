let promise = require('promise');
let UserSchema = require('../model/userModel');

let saveNewUser = (parameter)=>{
    return new promise((resolve, reject)=>{
        let userSchema = new UserSchema(parameter);
        userSchema.save()
        .then(function(savedUser){
            if(savedUser){
                resolve(savedUser);
            }else{
                reject('User not saved : '+err);
            }
            
        }).catch(function(err){
            reject('User not saved : '+err);
        })
    });
};


module.exports ={
    saveNewUser : saveNewUser
}