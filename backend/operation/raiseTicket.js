let promise = require('promise');
let TicketSchema = require('../model/ticket');

let raiseTicket = (parameter)=>{
    return new promise((resolve, reject)=>{
        let ticketSchema = new TicketSchema(parameter);
        ticketSchema.save()
        .then(function(savedTicket){
            if(savedTicket){
                resolve(savedTicket);
            }else{
                reject('Ticket not generated : '+err);
            }
            
        }).catch(function(err){
            reject('Ticket not generated : '+err);
        })
    });
};

let fetchAllTicketDetails = () => {
    return new promise((resolve, reject) => {
        TicketSchema.find()
            .exec()
            .then((tickets) => {
                if(tickets){
                    resolve(tickets);
                }else{
                    reject('Ticket not Found : '+err);
                }
                   
            }).catch(function(err){
                reject('Ticket not Found : '+err);
            })
    })
}

let fetchTicketDetailsOnStatus = () => {
    return new promise((resolve, reject) => {
        TicketSchema.find({ status: "open" })
            .exec()
            .then((tickets) => {
                if(tickets){
                    resolve(tickets);
                }else{
                    reject('Ticket not Found : '+err);
                }
                   
            }).catch(function(err){
                reject('Ticket not Found : '+err);
            })
    })
}


module.exports ={
    raiseTicket : raiseTicket,
    fetchAllTicketDetails : fetchAllTicketDetails,
    fetchTicketDetailsOnStatus : fetchTicketDetailsOnStatus
}