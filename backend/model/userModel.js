let mongoose = require('mongoose');
let schema = mongoose.Schema;
let userSchema = schema({

    user_name:{
        type:String,
        required:true,
    },
    user_password:{
        type:String,
        required:true,
    }
});

let User = module.exports = mongoose.model('User', userSchema);