let mongoose = require('mongoose');
let schema = mongoose.Schema;
let TicketRaissed = schema({

    subject:{
        type:String,
        required:true,
    },
    description:{
        type:String,
        required:true,
    },
    status:{
        type:String,
        required:true,
    }
});

let Ticket = module.exports = mongoose.model('Ticket', TicketRaissed);