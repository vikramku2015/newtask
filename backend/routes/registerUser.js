let express = require('express');
let router = express.Router();
let RegisterUser = require('../operation/registerOperation');

router.post('/newUser', function(req, res, next){
    
    let userDetails = {
        user_name : req.body.userName,
        user_password : req.body.password
    }
    RegisterUser.saveNewUser(userDetails).then(function(savedUser){
        if(savedUser){
            res.send({
                success : true,
                MSG : "User Successfullt Saved",
                savedUser : savedUser
            })
        }else{
            res.send({
                success : false,
                MSG : "User not saved"
            })
        }
    }).catch(function(err){
        if(err){
            res.send({
                success : false,
                MSG : "User not saved"
            })
        }
    })
})

module.exports = router;