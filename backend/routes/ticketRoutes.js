let express = require('express');
let router = express.Router();
let TicketOperation = require('../operation/raiseTicket');

/**
 * Fetching the comlete ticket details
 */
router.get('/ticketDetails',function(req, res, next){
    TicketOperation.fetchAllTicketDetails().then((ticketDetails)=>{
        res.send({
            success:true,
            MSG:'ticket details found',
            ticketData:ticketDetails
        })
    }).catch(function(err){
        if(err){
            res.send({
                success : false,
                MSG : "Ticket Not Found"
            })
        }
    })
});


router.get('/ticketDetailsOnStatus',function(req, res){
    TicketOperation.fetchTicketDetailsOnStatus().then((ticketDetailsOnStatus)=>{
        res.send({
            success:true,
            MSG:'ticket details found',
            ticketDetailsOnStatus:ticketDetailsOnStatus
        })
    }).catch(function(err){
        if(err){
            res.send({
                success : false,
                MSG : "Ticket Not Found"
            })
        }
    })
});

module.exports = router;