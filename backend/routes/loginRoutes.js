let express = require('express');
let router = express.Router();
let UserOperation = require('../operation/login');
let TicketOperation = require('../operation/raiseTicket');

router.post('/user', function(req, res, next){
    
    let userDetails = {
        userName : req.body.userName,
        password : req.body.password
    }
    
    UserOperation.checkUser(userDetails).then(function(matched){
        if(matched == true){
            let ticket = {
                subject : req.body.subject,
                description : req.body.description,
                status : 'open'
            }
            TicketOperation.raiseTicket(ticket).then(function(result){
                res.send({
                    success : true,
                    MSG : "User authentication matched and ticket raised",
                    result : result
                })
            })
        }else{
            res.send({
                success : false,
                MSG : "User authentication not matched"
            })
        }
    }).catch(function(err){
        if(err){
            res.send({
                success : false,
                MSG : "User Name Not matched"
            })
        }
    })
})

module.exports = router;