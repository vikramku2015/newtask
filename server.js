let express = require('express');
let mongoose = require('mongoose');
let bodyParser = require('body-parser');
let morgan = require('morgan');
let path = require('path');


// My generated routes
let config = require('./config/config');
let loginRoute = require('./backend/routes/loginRoutes');
let logOutRoute = require('./backend/routes/logoutRoutes');
let ticketRoute = require('./backend/routes/ticketRoutes');
let saveUser = require('./backend/routes/registerUser');


let app = express();

//middleware
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
// app.use(bodyParser());


//Routes
app.use('/login',loginRoute);
app.use('/logOut',logOutRoute);
app.use('/ticket',ticketRoute);
app.use('/register',saveUser);


// app.use((data,req, res, next) => {
//     const error = new Error(data);
//     error.status = 404;
//     next(error);
// });

// app.use((error, req, res) => {
//     res.status(error.status || 500);
//     res.send({
//         error: {
//             message: error.message,
//             status: error.status
//         }
//     })
// })



//server start
app.listen(config.port, function (err) {
    if (err) {
        console.log('error found in server start' + err);
    } else {
        console.log("connected to server at port " + config.port);
    }
});



//databse connectivity
mongoose.connect(config.database);
mongoose.connection.on("connected", function (err) {
    if (err) {
        console.log("error in database connectivity" + err);
    } else {
        console.log('connected to database at port 27017');
    }
});



